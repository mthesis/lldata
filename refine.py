import numpy as np

q1=np.load("../ldata/ldatajet_4mom_fromlep.npz")["jetsAll"]
q2=np.load("../ldata/ldatajet_4mom_fromlept.npz")["jetsAll"]

n=np.min([q1.shape[0],q2.shape[0]])

np.random.shuffle(q1)
np.random.shuffle(q2)

q1=q1[:n,:,:]
q2=q2[:n,:,:]

print(n)

np.savez_compressed("lep",q=q1)
np.savez_compressed("lept",q=q2)
